""" This function measures the elevation profile for a channel centerline vector (UTM projection) and DSM.

For each orthogonal transect (taken by default at 10 meter intervals) the standard deviation, median, 25th and 75th 
percentiles are taken to capture the interquartile range (IQR) of elevations along the long profile.
         
              (1)
               v
    Floodplain |<--- transect
    ___________|________________
               |
    > RIVER >==+========== <- centerline
    ___________|_______________
               |
    Floodplain | >->-> 10 meter interval (default)
               ^


     (1) Profile
        |                 .
        |....         ....
       Z|    ..    ...
        |      ....
        |__________________
                X

"""

__author__ = "Sam Brooke"
__email__ = "sbrooke@ucsb.edu"

import os
from osgeo import ogr
from osgeo import osr
import numpy as np
import csv
from shapely.geometry import Point, LineString, shape
import rasterio
import fiona
import math
import shapely
import argparse

parser = argparse.ArgumentParser(description='Generate a swath profile from a DSM')
parser.add_argument('lines', type=str, help='Shapefile or geojson containing line vectors (UTM only)')
parser.add_argument('dsm', type=str, help='Digital surface model to query (UTM only)')
parser.add_argument('width', type=float, help='Distance +- from the line to query')
parser.add_argument('csv_output', type=str, help='Elevation output data')
parser.add_argument('--interval', type=float, nargs='?', help='Query measurement interval (default=10m)', default=float(10))
parser.add_argument('--output-lines', type=str, nargs='?', help='Export transect lines to a geojson')
parser.add_argument('--nodata', type=int, nargs='?', help='Nodata value')

args = parser.parse_args()

def getAngle(pt1, pt2):
    x_diff = pt2.x - pt1.x
    y_diff = pt2.y - pt1.y
    return math.degrees(math.atan2(y_diff, x_diff))

## start and end points of chainage tick
## get the first end point of a tick
def getPoint1(pt, bearing, dist):
    angle = bearing + 90
    bearing = math.radians(angle)
    x = pt.x + dist * math.cos(bearing)
    y = pt.y + dist * math.sin(bearing)
    return Point(x, y)

## get the second end point of a tick
def getPoint2(pt, bearing, dist):
    bearing = math.radians(bearing)
    x = pt.x + dist * math.cos(bearing)
    y = pt.y + dist * math.sin(bearing)
    return Point(x, y)

def raster_values(x_list,y_list,band,affine):

    row, col = ~affine * (np.array(x_list), np.array(y_list)) # x, y to raster coords
    try:
        col = col.astype(int)
        row = row.astype(int)

        raster_vals = band[col,row]

        # Check distances
        coord_distances = []
        coord_distances.append(0)
        for n, x in enumerate(x_list):
            if n > 0:
                dist = math.hypot(x_list[0] - x, y_list[0] - y_list[n])
                coord_distances.append(dist)

        dx = np.diff(coord_distances),
        dy = np.diff(raster_vals)
        mx = np.transpose(dy/dx) # slope

    except IndexError:
        raster_vals = []
        coord_distances = []
        mx = 0

    return raster_vals, coord_distances, mx

print(args.lines)
with fiona.open(args.lines, 'r') as profiles:

    with rasterio.open(args.dsm) as source:
        band = source.read(1) # Read raster band 1 as a numpy array
        affine = source.profile['transform']

    driver = ogr.GetDriverByName('GeoJSON')
    srs = osr.SpatialReference()
    print('Using EPSG: '+profiles.crs['init'][5:])
    srs.ImportFromEPSG(int(profiles.crs['init'][5:]))

    if args.output_lines:
        qtl = driver.CreateDataSource(args.output_lines)
        query_transects = qtl.CreateLayer('', srs, ogr.wkbLineString)

    point_compass_elevations = []
    point_compass_labels = []

    total_profiles = len(list(profiles))

    for nx, profile in enumerate(profiles):

        print('Processing swaths: ' + str(nx+1) + '/' + str(total_profiles))
        ## distance between each points
        distance = args.interval
        ## the length of each tick
        tick_length = args.width

        ## list to hold all the point coords
        list_points = []
        point_distances = []
        ## set the current distance to place the point
        current_dist = distance
        ## get the geometry of the line as wkt

        shapely_line = shape(profile['geometry'])

        ## get the total length of the line
        line_length = shapely_line.length

        ## append the starting coordinate to the list
        first_coord = Point(profile['geometry']['coordinates'][0])
        last_coord = Point(profile['geometry']['coordinates'][-1])

        list_points.append(first_coord)

        point_distances.append(current_dist)

        while current_dist < line_length:
            ## use interpolate and increase the current distance
            list_points.append(shapely_line.interpolate(current_dist))
            current_dist += distance
            point_distances.append(current_dist)

        ## append end coordinate to the list
        list_points.append(last_coord)
        point_distances.append(current_dist)
        print(len(list_points))
        print(len(point_distances))

        if os.path.exists(args.csv_output):
            os.remove(args.csv_output)

        with open(args.csv_output, mode='w') as elev_data_out:

            elev_writer = csv.writer(elev_data_out, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

            sample_interval = 1 # Space between elevation measurements

            header = ['distance', 'median', 'stdev', 'prc_25', 'prc_75']
            elev_writer.writerow(header)

            for num, pt in enumerate(list_points, 1):

                print(str(num)+'/'+str(len(list_points)))
                ## start chainage 0
                if num == 1:
                    point_distance = point_distances[num]
                    current_point = list_points[num]
                    angle_1 = getAngle(pt, list_points[num])
                    line_end_1 = getPoint1(pt, angle_1, tick_length)
                    angle_2 = getAngle(line_end_1, pt)
                    line_end_2 = getPoint2(line_end_1, angle_2, tick_length*2)

                ## everything in between
                if num < len(list_points) - 1:
                    point_distance = point_distances[num]
                    current_point = list_points[num]
                    angle_1 = getAngle(pt, current_point)
                    line_end_1 = getPoint1(current_point, angle_1, tick_length)
                    angle_2 = getAngle(line_end_1, current_point)
                    line_end_2 = getPoint2(line_end_1, angle_2, tick_length*2)

                ## end chainage
                if num == len(list_points):
                    point_distance = point_distances[num-2]
                    current_point = list_points[num-2]
                    angle_1 = getAngle(list_points[num - 2], pt) # Angle from line to point
                    line_end_1 = getPoint1(pt, angle_1, tick_length)
                    angle_2 = getAngle(line_end_1, pt) # Angle from
                    line_end_2 = getPoint2(line_end_1, angle_2, tick_length*2)

                current_dist = sample_interval

                x_list = []
                y_list = []
                distances = []

                max_len = math.hypot(line_end_1.x - line_end_2.x, line_end_1.y - line_end_2.y) # distance

                query_line = LineString([(line_end_1.x, line_end_1.y),(line_end_2.x, line_end_2.y)])

                while current_dist < max_len:
                    ## use interpolate and increase the current distance
                    interp_point = shape(query_line).interpolate(current_dist)
                    d = list(interp_point.xy)
                    x_list.append(d[0][0])
                    y_list.append(d[1][0])
                    current_dist += sample_interval

                l1_elev, l1_dist, slope = raster_values(x_list,y_list,band,affine)

                if len(l1_elev):

                    l1_elev = np.array(l1_elev).astype(np.float32)
                    l1_elev[l1_elev==args.nodata]=np.nan
                    print(np.nanmedian(l1_elev))
                    csv_row = [point_distance, np.nanmedian(l1_elev), np.nanstd(l1_elev),
                        np.nanpercentile(l1_elev, 25), np.nanpercentile(l1_elev, 75)]

                    elev_writer.writerow(csv_row)

                    feat_dfn_line = query_transects.GetLayerDefn()
                    feat_line = ogr.Feature(feat_dfn_line)
                    feat_line.SetGeometry(ogr.CreateGeometryFromWkt(query_line.wkt))
                    query_transects.CreateFeature(feat_line)
                    feat_line.Destroy()
