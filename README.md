# Avulsion Flood Scour Length Calculations

This code repository contains the raw data, data analysis and plotting functions for the manuscript Brooke et al., 2020 (GRL) entitled **Flood magnitude and duration control the location of lobe-scale avulsions on deltas: Madagascar**.

## Prerequisites

Python 3 (e.g. [Anaconda](https://www.anaconda.com/distribution/))

[Jupyter Notebook](https://jupyter.org/install)

### Python Libraries

- gdal
- matplotlib
- pandas
- scipy
- numpy
- rasterio
- shapely
- fiona
- geopandas
- matplotlib_scalebar
- brokenaxes


### Install using pip

Jupyter Notebook:

```bash
pip install jupyterlab
```

Dependencies:

```bash
pip install matplotlib gdal pandas numpy scipy rasterio shapely fiona geopandas matplotlib_scalebar brokenaxes
```


### Install using Anaconda

Jupyter Notebook:

```bash
conda install -c conda-forge notebook
```

Dependencies:

```bash
conda install -c conda-forge gdal matplotlib pandas numpy scipy rasterio shapely fiona geopandas matplotlib_scalebar brokenaxes
```

### File Structure

**Madagascar Avulsion Locations:**

`./avulsion_locations/`

**Trunk Stream Vectors:**

`./trunk_streams_early_2000s/`

**Elevation Models:**

`./elevation_data/srtm_floodplains/`

**River Elevation Profiles (output)**:

`./profiles`

**River Profile Swath Transects (output)**:

`./transects`

**Floodplain Area Vectors**:

`./floodplains`

**Orthogonal transect vectors across rivers to measure upstream and downstream geometry:**

`./river_transects/width_vectors/`

**Surface Water Masks [[Pekel et al., 2016\]](https://global-surface-water.appspot.com/download):**

`/river_transects/river_water_masks/seasonal_cropped`

`./river_transects/river_water_masks/max_cropped`

**River Discharge Data [[GRDC](https://www.bafg.de/GRDC/EN/01_GRDC/13_dtbse/database_node.html) and [RivDis](https://daac.ornl.gov/RIVDIS/guides/rivdis_guide.html)]:**

`./discharge_data/`

### GRDC Data

Data from the GRDC database not provided in this repository, and must instead be requested/downloaded from (as per the usage agreement) [https://www.bafg.de/GRDC/EN/02_srvcs/21_tmsrs/210_prtl/prtl_node.html;jsessionid=3BBA87148D211D9E1F898FFF69C70CE1.live11291](https://www.bafg.de/GRDC/EN/02_srvcs/21_tmsrs/210_prtl/prtl_node.html;jsessionid=3BBA87148D211D9E1F898FFF69C70CE1.live11291).

The datasets we used from the GRDC library are listed in `./GRDC_data_requests/`


## Walkthrough

### 1. Start Jupyter Notebook

```bash
cd avulsion-flood-scour-paper
jupyter notebook
```

### 2. Process Width Swaths for Madagascar - For Channel Wbf

[./channel_widths.ipynb](https://gitlab.com/sambrooke/avulsion-flood-scour-paper/-/blob/master/channel_widths.ipynb)

### 3. Process Channel Profile Swaths for Madagascar from SRTM3 Elevation

1. Create bash script using [./create_river_swath_cmds.ipynb](https://gitlab.com/sambrooke/avulsion-flood-scour-paper/-/blob/master/create_river_swath_cmds.ipynb)

2. Run bash script (depends on IQR_profiler.py)

`bash madagascar_river_commands.sh`

### 4. Plot Channel Profiles and Collect Local Slope for Each River - For Slope

[./avulsion_distance_and_slopes.ipynb](https://gitlab.com/sambrooke/avulsion-flood-scour-paper/-/blob/master/avulsion_distance_and_slopes.ipynb)

### 5. Calculate Discharge Recurrence Intervals - For t_scour

Discharge data for Madagascar and global rivers were collected from the [RivDis](https://daac.ornl.gov/cgi-bin/dsviewer.pl?ds_id=199) (Vorosmarty et al., 1998) and [Global Runoff Data Base](https://www.bafg.de/GRDC/EN/01_GRDC/13_dtbse/database_node.html) (GRDC). In accordance to the latter's [data policy](https://www.bafg.de/GRDC/EN/01_GRDC/12_plcy/policy_guidelines.pdf?__blob=publicationFile), we are unable to distribute GRDC discharge data as part of this contribution. We instead supply a copy the list of stations we requested for both daily and monthly data.

​	***Global data (monthly and daily):*** [./GRDC_data_requests/global_rivers.csv](https://gitlab.com/sambrooke/avulsion-flood-scour-paper/-/blob/master/GRDC_data_requests/global_rivers.csv)

​		Saved to `./discharge_data/global_month_only` and`./discharge_data/global_day_only`

​	***Madagascar data***: [./GRDC_data_requests/madagascar_rivers.csv](https://gitlab.com/sambrooke/avulsion-flood-scour-paper/-/blob/master/GRDC_data_requests/madagascar_rivers.csv)

​		Saved to  `./discharge_data/GRDC_madagascar_monthly`

With the data create discharge recurrence table using [./discharge_recurrence_intervals.ipynb](https://gitlab.com/sambrooke/avulsion-flood-scour-paper/-/blob/master/discharge_recurrence_intervals.ipynb)

#### 6. Calculate and Plot Sediment Transport Capacity Change (Fans) -- not in paper

[./qs_capacity_calcs.ipynb](https://gitlab.com/sambrooke/avulsion-flood-scour-paper/-/blob/master/qs_capacity_calcs.ipynb)

#### 7. Calculate and Plot Flood Scour Lengths - l_scour, Figure 3a and c

##### Month data only
[./l_scour_calculations_Monthly_Only.ipynb](https://gitlab.com/sambrooke/avulsion-flood-scour-paper/-/blob/master/l_scour_calculations_Monthly_Only.ipynb)

#### 8. Te* calculations - Figure 3b

[./Plot_Te_star.ipynb](https://gitlab.com/sambrooke/avulsion-flood-scour-paper/-/blob/master/Plot_Te_star.ipynb)

#### 9. Compare Daily vs Monthly l_scour lengths -- in supplementary

[./Compare_Daily_vs_Monthly_L_scour.ipynb](https://gitlab.com/sambrooke/avulsion-flood-scour-paper/-/blob/master/Compare_Daily_vs_Monthly_L_scour.ipynb)

### Data and Figures

Saved to `./figures`

Saved to `./scour_data_output`

Final table --> `./scour_data_output/l_scour_table_monthly_only.csv`
