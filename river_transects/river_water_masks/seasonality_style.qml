<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis hasScaleBasedVisibilityFlag="0" version="3.8.0-Zanzibar" maxScale="0" styleCategories="AllStyleCategories" minScale="1e+8">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <customproperties>
    <property value="false" key="WMSBackgroundLayer"/>
    <property value="false" key="WMSPublishDataSourceUrl"/>
    <property value="0" key="embeddedWidgets/count"/>
    <property value="Value" key="identify/format"/>
  </customproperties>
  <pipe>
    <rasterrenderer type="paletted" opacity="1" band="1" alphaBand="-1">
      <rasterTransparency/>
      <minMaxOrigin>
        <limits>None</limits>
        <extent>WholeRaster</extent>
        <statAccuracy>Estimated</statAccuracy>
        <cumulativeCutLower>0.02</cumulativeCutLower>
        <cumulativeCutUpper>0.98</cumulativeCutUpper>
        <stdDevFactor>2</stdDevFactor>
      </minMaxOrigin>
      <colorPalette>
        <paletteEntry alpha="255" color="#ffffff" value="0" label="0"/>
        <paletteEntry alpha="255" color="#8cc6e4" value="1" label="1"/>
        <paletteEntry alpha="255" color="#7fb4df" value="2" label="2"/>
        <paletteEntry alpha="255" color="#72a2da" value="3" label="3"/>
        <paletteEntry alpha="255" color="#6690d4" value="4" label="4"/>
        <paletteEntry alpha="255" color="#597ecf" value="5" label="5"/>
        <paletteEntry alpha="255" color="#4c6cca" value="6" label="6"/>
        <paletteEntry alpha="255" color="#3f5ac4" value="7" label="7"/>
        <paletteEntry alpha="255" color="#3348bf" value="8" label="8"/>
        <paletteEntry alpha="255" color="#2636ba" value="9" label="9"/>
        <paletteEntry alpha="255" color="#1924b4" value="10" label="10"/>
        <paletteEntry alpha="255" color="#0c12af" value="11" label="11"/>
        <paletteEntry alpha="255" color="#0000aa" value="12" label="12"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="13" label="13"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="14" label="14"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="15" label="15"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="16" label="16"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="17" label="17"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="18" label="18"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="19" label="19"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="20" label="20"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="21" label="21"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="22" label="22"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="23" label="23"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="24" label="24"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="25" label="25"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="26" label="26"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="27" label="27"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="28" label="28"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="29" label="29"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="30" label="30"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="31" label="31"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="32" label="32"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="33" label="33"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="34" label="34"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="35" label="35"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="36" label="36"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="37" label="37"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="38" label="38"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="39" label="39"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="40" label="40"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="41" label="41"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="42" label="42"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="43" label="43"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="44" label="44"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="45" label="45"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="46" label="46"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="47" label="47"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="48" label="48"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="49" label="49"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="50" label="50"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="51" label="51"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="52" label="52"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="53" label="53"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="54" label="54"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="55" label="55"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="56" label="56"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="57" label="57"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="58" label="58"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="59" label="59"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="60" label="60"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="61" label="61"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="62" label="62"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="63" label="63"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="64" label="64"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="65" label="65"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="66" label="66"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="67" label="67"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="68" label="68"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="69" label="69"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="70" label="70"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="71" label="71"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="72" label="72"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="73" label="73"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="74" label="74"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="75" label="75"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="76" label="76"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="77" label="77"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="78" label="78"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="79" label="79"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="80" label="80"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="81" label="81"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="82" label="82"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="83" label="83"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="84" label="84"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="85" label="85"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="86" label="86"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="87" label="87"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="88" label="88"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="89" label="89"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="90" label="90"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="91" label="91"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="92" label="92"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="93" label="93"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="94" label="94"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="95" label="95"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="96" label="96"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="97" label="97"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="98" label="98"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="99" label="99"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="100" label="100"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="101" label="101"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="102" label="102"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="103" label="103"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="104" label="104"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="105" label="105"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="106" label="106"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="107" label="107"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="108" label="108"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="109" label="109"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="110" label="110"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="111" label="111"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="112" label="112"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="113" label="113"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="114" label="114"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="115" label="115"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="116" label="116"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="117" label="117"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="118" label="118"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="119" label="119"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="120" label="120"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="121" label="121"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="122" label="122"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="123" label="123"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="124" label="124"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="125" label="125"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="126" label="126"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="127" label="127"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="128" label="128"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="129" label="129"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="130" label="130"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="131" label="131"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="132" label="132"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="133" label="133"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="134" label="134"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="135" label="135"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="136" label="136"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="137" label="137"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="138" label="138"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="139" label="139"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="140" label="140"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="141" label="141"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="142" label="142"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="143" label="143"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="144" label="144"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="145" label="145"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="146" label="146"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="147" label="147"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="148" label="148"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="149" label="149"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="150" label="150"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="151" label="151"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="152" label="152"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="153" label="153"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="154" label="154"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="155" label="155"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="156" label="156"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="157" label="157"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="158" label="158"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="159" label="159"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="160" label="160"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="161" label="161"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="162" label="162"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="163" label="163"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="164" label="164"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="165" label="165"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="166" label="166"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="167" label="167"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="168" label="168"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="169" label="169"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="170" label="170"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="171" label="171"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="172" label="172"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="173" label="173"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="174" label="174"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="175" label="175"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="176" label="176"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="177" label="177"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="178" label="178"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="179" label="179"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="180" label="180"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="181" label="181"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="182" label="182"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="183" label="183"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="184" label="184"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="185" label="185"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="186" label="186"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="187" label="187"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="188" label="188"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="189" label="189"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="190" label="190"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="191" label="191"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="192" label="192"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="193" label="193"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="194" label="194"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="195" label="195"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="196" label="196"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="197" label="197"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="198" label="198"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="199" label="199"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="200" label="200"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="201" label="201"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="202" label="202"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="203" label="203"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="204" label="204"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="205" label="205"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="206" label="206"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="207" label="207"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="208" label="208"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="209" label="209"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="210" label="210"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="211" label="211"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="212" label="212"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="213" label="213"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="214" label="214"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="215" label="215"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="216" label="216"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="217" label="217"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="218" label="218"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="219" label="219"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="220" label="220"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="221" label="221"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="222" label="222"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="223" label="223"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="224" label="224"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="225" label="225"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="226" label="226"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="227" label="227"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="228" label="228"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="229" label="229"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="230" label="230"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="231" label="231"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="232" label="232"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="233" label="233"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="234" label="234"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="235" label="235"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="236" label="236"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="237" label="237"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="238" label="238"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="239" label="239"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="240" label="240"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="241" label="241"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="242" label="242"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="243" label="243"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="244" label="244"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="245" label="245"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="246" label="246"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="247" label="247"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="248" label="248"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="249" label="249"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="250" label="250"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="251" label="251"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="252" label="252"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="253" label="253"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="254" label="254"/>
        <paletteEntry alpha="255" color="#c0c0c0" value="255" label="255"/>
      </colorPalette>
      <colorramp type="randomcolors" name="[source]"/>
    </rasterrenderer>
    <brightnesscontrast contrast="0" brightness="0"/>
    <huesaturation colorizeStrength="100" colorizeGreen="128" colorizeRed="255" saturation="0" colorizeOn="0" colorizeBlue="128" grayscaleMode="0"/>
    <rasterresampler maxOversampling="2"/>
  </pipe>
  <blendMode>0</blendMode>
</qgis>
