

STREAM=./trunk_streams_early_2000s/betsiboka.shp
DSM=./elevation_data/srtm_floodplains/betsiboka.tif
WIDTH=1000
INTERVAL=10
OUTPUT=./profiles/betsiboka_profile.csv
OUTPUT_LINES=./transects/betsiboka_transects.geojson
NODATA=0
python3 IQR_profiler.py $STREAM $DSM $WIDTH $OUTPUT --interval $INTERVAL --output-lines $OUTPUT_LINES --nodata $NODATA

        

STREAM=./trunk_streams_early_2000s/manambolo.shp
DSM=./elevation_data/srtm_floodplains/manambolo.tif
WIDTH=1000
INTERVAL=10
OUTPUT=./profiles/manambolo_profile.csv
OUTPUT_LINES=./transects/manambolo_transects.geojson
NODATA=0
python3 IQR_profiler.py $STREAM $DSM $WIDTH $OUTPUT --interval $INTERVAL --output-lines $OUTPUT_LINES --nodata $NODATA

        

STREAM=./trunk_streams_early_2000s/mahajamba.shp
DSM=./elevation_data/srtm_floodplains/mahajamba.tif
WIDTH=1000
INTERVAL=10
OUTPUT=./profiles/mahajamba_profile.csv
OUTPUT_LINES=./transects/mahajamba_transects.geojson
NODATA=32767
python3 IQR_profiler.py $STREAM $DSM $WIDTH $OUTPUT --interval $INTERVAL --output-lines $OUTPUT_LINES --nodata $NODATA

        

STREAM=./trunk_streams_early_2000s/fiherenana.shp
DSM=./elevation_data/srtm_floodplains/fiherenana.tif
WIDTH=1000
INTERVAL=10
OUTPUT=./profiles/fiherenana_profile.csv
OUTPUT_LINES=./transects/fiherenana_transects.geojson
NODATA=0
python3 IQR_profiler.py $STREAM $DSM $WIDTH $OUTPUT --interval $INTERVAL --output-lines $OUTPUT_LINES --nodata $NODATA

        

STREAM=./trunk_streams_early_2000s/sambao.shp
DSM=./elevation_data/srtm_floodplains/sambao.tif
WIDTH=1000
INTERVAL=10
OUTPUT=./profiles/sambao_profile.csv
OUTPUT_LINES=./transects/sambao_transects.geojson
NODATA=0
python3 IQR_profiler.py $STREAM $DSM $WIDTH $OUTPUT --interval $INTERVAL --output-lines $OUTPUT_LINES --nodata $NODATA

        

STREAM=./trunk_streams_early_2000s/morondava.shp
DSM=./elevation_data/srtm_floodplains/morondava.tif
WIDTH=1000
INTERVAL=10
OUTPUT=./profiles/morondava_profile.csv
OUTPUT_LINES=./transects/morondava_transects.geojson
NODATA=32767
python3 IQR_profiler.py $STREAM $DSM $WIDTH $OUTPUT --interval $INTERVAL --output-lines $OUTPUT_LINES --nodata $NODATA

        

STREAM=./trunk_streams_early_2000s/mangoky.shp
DSM=./elevation_data/srtm_floodplains/mangoky.tif
WIDTH=1000
INTERVAL=10
OUTPUT=./profiles/mangoky_profile.csv
OUTPUT_LINES=./transects/mangoky_transects.geojson
NODATA=0
python3 IQR_profiler.py $STREAM $DSM $WIDTH $OUTPUT --interval $INTERVAL --output-lines $OUTPUT_LINES --nodata $NODATA

        